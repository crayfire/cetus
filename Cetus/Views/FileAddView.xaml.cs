﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cetus
{
    /// <summary>
    /// Interaktionslogik für FileAdd.xaml
    /// </summary>
    public partial class FileAddView : UserControl
    {
        private FileAddViewModel viewModel = new FileAddViewModel();
        public FileAddView()
        {
            InitializeComponent();

            this.viewModel = new FileAddViewModel();
            this.DataContext = viewModel;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton a = (RadioButton)sender;
            if (a.Tag.ToString() == "p" && p1 != null)
            {
                //MessageBox.Show("Privat");
                p1.Visibility = Visibility.Visible;
                p2.Visibility = Visibility.Visible;
                p3.Visibility = Visibility.Visible;
                p4.Visibility = Visibility.Visible;
                p5.Visibility = Visibility.Visible;
                c1.Visibility = Visibility.Hidden;
                c2.Visibility = Visibility.Hidden;
                c3.Visibility = Visibility.Hidden;
                c4.Visibility = Visibility.Hidden;

            }
            else if (a.Tag.ToString() == "c")
            {
                //MessageBox.Show("Firma");
                p1.Visibility = Visibility.Hidden;
                p2.Visibility = Visibility.Hidden;
                p3.Visibility = Visibility.Hidden;
                p4.Visibility = Visibility.Hidden;
                p5.Visibility = Visibility.Hidden;
                c1.Visibility = Visibility.Visible;
                c2.Visibility = Visibility.Visible;
                c3.Visibility = Visibility.Visible;
                c4.Visibility = Visibility.Visible;

            }
        }

    }
}
