﻿using System.Windows;
using System.Windows.Controls;

namespace Cetus
{
    /// <summary>
    /// Interaktionslogik für MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
            this.DataContext = new MainViewModel();

        }

    }

}
