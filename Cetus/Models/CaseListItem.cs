﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cetus
{
    public class CaseListItem
    {
        public int CaseID { get; set; }
        public string CaseName { get; set; }
        public string CaseStatus { get; set; }
        public DateTime CaseDate { get; set; }
        public string CaseVehicle { get; set; }
        public string CaseContact { get; set; }
        public string ExpertName { get; set; }
    }
}
