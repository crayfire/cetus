﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cetus
{
    public class DocumentTemplateItemModel : INotifyPropertyChanged
    {
        private int _rowid;
        public int ROWID
        {
            get { return _rowid; }
            set
            {
                if (_rowid != value)
                {
                    _rowid = value;
                    OnPropertyChanged("ROWID");
                }
            }
        }

        private string _FileName = null;
        public string FileName
        {
            get { return _FileName; }
            set
            {
                if (_FileName != value)
                {
                    _FileName = value;
                    OnPropertyChanged("FileName");
                }
            }
        }

        private int _FileSize;
        public int FileSize
        {
            get { return _FileSize; }
            set
            {
                if (_FileSize != value)
                {
                    _FileSize = value;
                    OnPropertyChanged("FileSize");
                }
            }
        }

        private int _FileCreated;
        public int FileCreated
        {
            get { return _FileCreated; }
            set
            {
                if (_FileCreated != value)
                {
                    _FileCreated = value;
                    OnPropertyChanged("FileCreated");
                }
            }
        }

        private int _FileChanged;
        public int FileChanged
        {
            get { return _FileChanged; }
            set
            {
                if (_FileChanged != value)
                {
                    _FileChanged = value;
                    OnPropertyChanged("FileChanged");
                }
            }
        }

        private string _FilePath = null;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if (_FilePath != value)
                {
                    _FilePath = value;
                    OnPropertyChanged("FilePath");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }
    }
}
