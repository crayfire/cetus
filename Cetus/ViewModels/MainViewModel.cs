﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Cetus
{

    public class NavigationItemModel
    {
        public string Title { get; set; }
        public string IconGlyph { get; set; }
        public UserControl Page { get; set; }
    }

    public class MainViewModel : ViewModelBase
    {
        public DelegateCommand FileListCommand { get; private set; }
        public DelegateCommand FileAddCommand { get; private set; }
        public DelegateCommand ContactListCommand { get; private set; }
        public DelegateCommand ContactAddCommand { get; private set; }
        public DelegateCommand DocumentListCommand { get; private set; }
        public DelegateCommand DocumentAddCommand { get; private set; }
        public DelegateCommand DocumentTemplateListCommand { get; private set; }
        public DelegateCommand DocumentTemplateAddCommand { get; private set; }


        public MainViewModel()
        {
            this.FileListCommand = new DelegateCommand(FileListAction);
            this.FileAddCommand = new DelegateCommand(FileAddAction);
            this.ContactListCommand = new DelegateCommand(ContactListAction);
            this.ContactAddCommand = new DelegateCommand(ContactAddAction);
            this.DocumentListCommand = new DelegateCommand(DocumentListAction);
            this.DocumentAddCommand = new DelegateCommand(DocumentAddAction);
            DocumentTemplateListCommand = new DelegateCommand(DocumentTemplateListAction);
            DocumentTemplateAddCommand = new DelegateCommand(DocumentTemplateAddAction);

        }

        private void DocumentAddAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new TelerikEditorViewModel();
            rw.Content = new TelerikEditor();
            rw.Name = "ContactAddWindow";
            rw.Width = 550;
            rw.MaxWidth = 550;
            rw.MinWidth = 550;
            rw.Height = 850;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Dokument erstellen";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }

        private void DocumentListAction(object obj)
        {
            throw new NotImplementedException();
        }

        private void ContactAddAction(object obj)
        {
            throw new NotImplementedException();
        }

        private void ContactListAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new ContactViewModel();
            rw.Content = new ContactView();
            rw.Name = "ContactListWindow";
            rw.Width = 850;
            rw.Height = 550;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Kontakte anzeigen";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }

        private void FileAddAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new FileAddViewModel();
            rw.Content = new FileAddView();
            rw.Name = "ContactAddWindow";
            rw.Width = 1200;
            rw.Height = 800;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Vorgang anlegen";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }

        private void FileListAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new FileListViewModel();
            rw.Content = new FileListView();
            rw.Name = "FileListWindow";
            rw.Width = 850;
            rw.Height = 550;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Vorgänge  auflisten";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }

        private void DocumentTemplateListAction (object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new DocumentTemplateViewModel();
            rw.Content = new DocumentTemplateView();
            rw.Name = "DocumentTemplateListWindow";
            rw.Width = 850;
            rw.Height = 550;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Vorlagen  auflisten";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }

        private void DocumentTemplateAddAction (object obj)
        {
            throw new NotImplementedException();
        }
    }

}
