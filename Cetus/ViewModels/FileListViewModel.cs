﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Cetus
{
    class FileListViewModel : ViewModelBase
    {
        private ObservableCollection<CaseListItem> _CaseListItems;
        public ObservableCollection<CaseListItem> CaseListItems
        {
            get
            {
                if (this._CaseListItems == null)
                {
                    this._CaseListItems = this.CreateCaseListItems();
                }

                return this._CaseListItems;
            }
        }
        private ObservableCollection<CaseListItem> CreateCaseListItems()
        {
            ObservableCollection<CaseListItem> _CaseListItems = new ObservableCollection<CaseListItem>();
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            _CaseListItems = DB.SelectCaseList();


            return _CaseListItems;
        }

    }
}