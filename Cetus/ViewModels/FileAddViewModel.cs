﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Controls;

namespace Cetus
{
    class FileAddViewModel : ViewModelBase
    {
        public DelegateCommand FileSaveCommand { get; private set; }
        public DelegateCommand FileAbortCommand { get; private set; }
        
        private ObservableCollection<AdressBookItemModel> _VisitPlaceAdressBook = new ObservableCollection<AdressBookItemModel>();
        private ObservableCollection<AdressBookItemModel> _GarageAdressBook = new ObservableCollection<AdressBookItemModel>();
        private ObservableCollection<AdressBookItemModel> _LawyerAdressBook = new ObservableCollection<AdressBookItemModel>();
        public ObservableCollection<AdressBookItemModel> VisitPlaceAdressBook
        {
            get
            {
                return this._VisitPlaceAdressBook;
            }
        }
        public ObservableCollection<AdressBookItemModel> GarageAdressBook
        {
            get
            {
                return this._GarageAdressBook;
            }
        }
        public ObservableCollection<AdressBookItemModel> LawyerAdressBook
        {
            get
            {
                return this._LawyerAdressBook;
            }
        }
        public FileAddViewModel()
        {
            this.FileSaveCommand = new DelegateCommand(FileSaveAction);
            this.FileAbortCommand = new DelegateCommand(FileAbortAction);

            LoadVisitPlaceAdressBook();
            LoadGarageAdressBook();
            LoadLawyerAdressBook();

        }

        private void FileAbortAction(object obj)
        {
            Window CurrentModalWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            CurrentModalWindow.Close();
            CurrentModalWindow = null;
        }

        private void FileSaveAction(object obj)
        {
            Window CurrentModalWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            CurrentModalWindow.Close();
            CurrentModalWindow = null;
        }

        private void LoadGarageAdressBook()
        {
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            _GarageAdressBook = DB.SelectAdressBook("garage");

        }
        private void LoadVisitPlaceAdressBook()
        {
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            _VisitPlaceAdressBook = DB.SelectAdressBook("garage");

        }

        private void LoadLawyerAdressBook()
        {
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            _LawyerAdressBook = DB.SelectAdressBook("lawyer");

        }
    }
}
