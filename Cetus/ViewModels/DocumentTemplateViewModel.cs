﻿using System.Collections.ObjectModel;
using Telerik.Windows.Controls;

namespace Cetus
{
    public class DocumentTemplateViewModel  : ViewModelBase
    {
        private ObservableCollection<DocumentTemplateItemModel> _DocumentTemplateItems;
        public ObservableCollection<DocumentTemplateItemModel> DocumentTemplateItems
        {
            get
            {
                if (_DocumentTemplateItems == null)
                {
                    this._DocumentTemplateItems = this.CreateDocumentTemplateItems();
                }

                return this.DocumentTemplateItems;
            }
        }

        public DocumentTemplateViewModel()
        {
            CreateDocumentTemplateItems();
        }

        private ObservableCollection<DocumentTemplateItemModel> CreateDocumentTemplateItems()
        {
            ObservableCollection<DocumentTemplateItemModel> _DocumentTemplateItems = new ObservableCollection<DocumentTemplateItemModel>();
            CrayfireDocumentsSQLite DB = new CrayfireDocumentsSQLite();
            _DocumentTemplateItems = DB.SelectDocumentTemplate();


            return _DocumentTemplateItems;
        }

    }
}