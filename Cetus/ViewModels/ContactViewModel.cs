﻿using System.Collections.ObjectModel;

namespace Cetus
{
    public class ContactViewModel
    {
        public ContactViewModel()
        {
            CreateAdressBookItems();
        }

        private ObservableCollection<AdressBookItemModel> _AdressBookItems;
        public ObservableCollection<AdressBookItemModel> AdressBookItems
        {
            get
            {
                if (this._AdressBookItems == null)
                {
                    this._AdressBookItems = this.CreateAdressBookItems();
                }

                return this._AdressBookItems;
            }
        }
        private ObservableCollection<AdressBookItemModel> CreateAdressBookItems()
        {
            ObservableCollection<AdressBookItemModel> _AdressBookItems = new ObservableCollection<AdressBookItemModel>();
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            _AdressBookItems = DB.SelectAdressBook();


            return _AdressBookItems;
        }
    }
}
