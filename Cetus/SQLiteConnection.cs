﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cetus
{
    public class CetusSQLiteConnection
    {
        private string dataSource = "cetus.db";
        private SQLiteConnection connection;
        private SQLiteCommand command;

        public CetusSQLiteConnection(){}

        private void Open()
        {
            connection = new SQLiteConnection();
            connection.ConnectionString = "Data Source=" + dataSource;
            connection.Open();
        }

        private void Close()
        {
            connection.Close();
            connection.Dispose();
        }

        public SQLiteDataReader Select(String text)
        {
            Open();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            command.CommandText = text;
            SQLiteDataReader reader = command.ExecuteReader();
            Close();
            return reader;
        }

        public ObservableCollection<CaseListItem> SelectCaseList()
        {
            ObservableCollection<CaseListItem> AdressBook = new ObservableCollection<CaseListItem>();
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "SELECT r.reportID, r.reportName, r.reportDate, r.status, c.firstname, c.lastname, v.licensePlate, v.vin, v.manufacturer, v.model FROM reports r LEFT JOIN vehicles v ON r.reportID = v.vehicleID LEFT JOIN contacts c ON r.reportID = c.contactID AND c.type = 'AS'";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                AdressBook.Add(
                                new CaseListItem
                                {
                                    CaseID = int.Parse(reader["reportID"].ToString()),
                                    CaseName = reader["reportName"].ToString(),
                                    CaseStatus = reader["status"].ToString(),
                                    //CaseDate = reader[3],
                                    CaseVehicle = reader["manufacturer"].ToString() + " " + reader["model"].ToString(),
                                    CaseContact = reader["lastname"].ToString() + ", " + reader["firstname"].ToString(),
                                    ExpertName = "",
                                });
            }
            Close();
            return AdressBook;
        }

        public ObservableCollection<AdressBookItemModel> SelectAdressBook(string adresstype = null)
        {
        ObservableCollection<AdressBookItemModel> AdressBook = new ObservableCollection<AdressBookItemModel>();
        Open();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            if (adresstype == null)
            {
            command.CommandText = "SELECT * FROM adressbook";
            }
            else
            {
                command.CommandText = "SELECT * FROM adressbook WHERE adresstype='"+ adresstype+"'";
            }

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                AdressBook.Add(new AdressBookItemModel
                {
                    FullName = reader[1].ToString() + " " + reader[2].ToString(),
                    FirstName = reader[1].ToString(),
                    LastName = reader[2].ToString(),
                    FullAdress = reader[3].ToString() + " " + reader[4].ToString() + ",\n" + reader[5].ToString() + " " + reader[6].ToString(),
                    StreetName = reader[3].ToString(),
                    StreetNumber = reader[4].ToString(),
                    ZipCode = reader[5].ToString(),
                    City = reader[6].ToString(),
                    Phone = reader[7].ToString(),
                    Fax = reader[8].ToString(),
                    Mobile = reader[9].ToString(),
                    Email = reader[10].ToString(),
                    Website = reader[11].ToString()
                });
            }
            Close();
            return AdressBook;
        }
        //SELECT r.reportID, r.reportName, r.reportDate, r.status, c.firstname, c.lastname, v.licensePlate, v.vin, v.manufacturer, v.model  FROM reports r, contacts c, vehicles v WHERE c.contactID = r.reportID AND c.type="AS" AND v.vehicleID = r.reportID
    }
}
