﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Collections.ObjectModel;

namespace Cetus
{
    public class CrayfireDocumentsSQLite
    {
        private string dataSource = "CetusTemplates.db";
        private SQLiteConnection connection;
        private SQLiteCommand command;

        public CrayfireDocumentsSQLite() { }

        private void Open()
        {
            connection = new SQLiteConnection();
            connection.ConnectionString = "Data Source=" + dataSource;
            connection.Open();
        }

        private void Close()
        {
            connection.Close();
            connection.Dispose();
        }

        public bool InsertDocumentTemplateItem(DocumentTemplateItemModel item)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "INSERT INTO DocmentTemplates(rowid,FileName,FileSize,FileCreated,FileChanged,FilePath) " +
                "VALUES ('" + null + "','" + item.FileName + "','" + item.FileSize + "','" + item.FileCreated + "','" + item.FileChanged + "','" + item.FilePath + "')";
            command.ExecuteNonQuery();
            Close();
            return true;
        }

        public bool UpdateDocumentTemplateItem(DocumentTemplateItemModel item)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "UPDATE DocmentTemplates SET " +
                "FileName='" + item.FileName.Trim() + "'," +
                "FileSize='" + item.FileSize + "'," +
                "FileChanged='" + item.FileChanged + "'," +
                "FilePath='" + item.FilePath + "'," +
                "WHERE rowid='" + item.ROWID + "'";
            command.ExecuteNonQuery();
            Close();
            return true;
        }

        public bool DeleteDocumentTemplateItem(int ROWID)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "DELETE FROM DucmentTemplates WHERE rowid ='" + ROWID + "'";
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
                return false;
                //throw;
            }
        }

        public ObservableCollection<DocumentTemplateItemModel> SelectDocumentTemplate()
        {
            ObservableCollection<DocumentTemplateItemModel> documentTemplate = new ObservableCollection<DocumentTemplateItemModel>();
            Open();
            command = new SQLiteCommand(connection);

            command.CommandText = "SELECT rowid,* FROM DocumentTemplates";

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                documentTemplate.Add(new DocumentTemplateItemModel
                {
                    FileName = reader["FileName"].ToString(),
                    FileSize = int.Parse(reader["FileSize"].ToString()),
                    FileCreated = int.Parse(reader["FileCreated"].ToString()),
                    FileChanged = int.Parse(reader["FileChanged"].ToString()),
                    FilePath = reader["FilePath"].ToString(),
                    ROWID = int.Parse(reader["rowid"].ToString())
                });
            }
            Close();
            return documentTemplate;
        }
        
        public DocumentTemplateItemModel SelectDocumentTeplateItem(int ROWID)
        {
            Open();
            DocumentTemplateItemModel documentTemplate = new DocumentTemplateItemModel();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            command.CommandText = "SELECT rowid, FileName, FileSize, FileCreated, FileChanged, FilePath FROM DocmentTemplates WHERE rowid='" + ROWID + "' LIMIT 1";

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                documentTemplate = new DocumentTemplateItemModel
                {
                    FileName = reader["FileName"].ToString(),
                    FileSize = int.Parse( reader["FileSize"].ToString()),
                    FileCreated = int.Parse(reader["FileCreated"].ToString()),
                    FileChanged = int.Parse(reader["FileChanged"].ToString()),
                    FilePath = reader["FilePath"].ToString(),
                    ROWID = int.Parse(reader["rowid"].ToString())
                };

            }
            return documentTemplate;
        }
    }
}
