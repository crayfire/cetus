﻿using System.Diagnostics;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Navigation;

namespace Cetus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            RadWindowInteropHelper.SetShowInTaskbar(this, true);
            //nextsoftgo go = new nextsoftgo();
            //go.CreateXMLObject();

        }

        private void MainView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}
